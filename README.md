# Docker Support

These are Dockerfiles and supporting infrastructure for common Docker needs I have.

## Maven AWS

Build:

```
docker build -t davemx/maven-aws:3-jdk-11 maven-aws
```

Run (with interactive bash):

```
docker run -it --rm -t davemx/maven-aws:3-jdk-11 bash
```

Push to Docker Hub:

```
docker push davemx/maven-aws:3-jdk-11
```
